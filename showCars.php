<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $connection = mysqli_connect('localhost', 'phpuser', 'phpuserpw', 'new');
        if (!$connection) {
            exit('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
        }
        mysqli_set_charset($connection, 'utf-8');

        $driver = mysqli_real_escape_string($connection, $_GET['driver']);
        $result = mysqli_query($connection, "SELECT Made, Number, Capacity
            FROM drivers
                JOIN links
                    ON drivers.id_Drivers = links.id_Drivers
                JOIN cars
                    ON cars.id_Car = links.id_Car
            WHERE drivers.FIO = '" .$driver ."'");
        if(!mysqli_num_rows($result)){
            exit("Водитель '" .$driver. "' не найден. Пожалуйста, проверьте правильность ввода и попробуйте еще раз.");
        }
        ?>
        
        <table border="black">
            <tr>
                <th>Марка</th>
                <th>Номер</th>
                <th>Вместимость</th>
            </tr>
            
        <?php
        echo "За водителем '".htmlentities($driver)."' закреплены следующие автомобили:"."<br>";
        while($row = mysqli_fetch_array($result)){
            echo "<tr><td>" . htmlentities($row['Made']) . "</td>";
            echo "<td>" . htmlentities($row['Number']) . "</td>";
            echo "<td>" . htmlentities($row['Capacity']) . "</td></tr>\n";
        }
        mysqli_free_result($result);
        mysqli_close($connection);
        ?>
        </table>
    </body>
</html>
